var express = require('express')
  , app = express();

var users = require('../routes/users');
var upload = require('../routes/upload');
var auth = require('../routes/auth');

var path = require('path');

app.get('/', function (req, res) {
	res.sendFile(path.resolve('public', 'index.html'))
})


app.use('/headerData/', users);
app.use('/upload/', upload);
app.use('/auth/', auth);

app.set('views', "./views");
app.set('view engine', 'html');
app.use(express.static('public'));
app.use('/static', express.static('files'));

module.exports = app