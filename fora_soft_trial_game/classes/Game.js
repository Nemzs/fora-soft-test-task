const gameStatus = {WAITING: "waiting", CHOOSING: "choosing", RESULTS: "results"};

class Game {
    constructor(gameRoom, socketId) {
        this.game = {
            connectedPlayers: 0,
            gameRoom: gameRoom,
            hostId: socketId,
            firstPlayer: '',
            secondPlayer: '',
            status: gameStatus.WAITING,
            firstPlayerReady: true,
            secondPlayerReady: false,
            score: [0, 0]
        };
    }

    getRoomName() {
        return this.game.gameRoom;
    }

    roomIsFull() {
        return this.game.connectedPlayers > 0;
    }

    playerConnected(clientId) {
        this.game.connectedPlayers++;
        this.game.joinedId = clientId;
        this.game.status = gameStatus.CHOOSING;
        this.game.secondPlayerReady = false;
        this.game.firstPlayerReady = false;
    }

    playerDisconnected() {
        this.game.firstPlayer = "";
        this.game.secondPlayer = "";
        this.game.secondPlayerReady = false;
        this.game.status = gameStatus.WAITING;
        this.game.connectedPlayers--;
    }

    getData() {
        return this.game;
    }

    setGesture(type, socketId) {
        if (this.game.hostId === socketId) {
            this.game.firstPlayer = type;
        } else {
            this.game.secondPlayer = type;
        }
        if (this.game.firstPlayer.length > 0 && this.game.secondPlayer.length > 0 && this.game.status === gameStatus.CHOOSING) {
            const firstPlayerWin = function (firstPlayerGesture, secondPlayerGesture) {
                const gestureBeats = {
                    "paper": ["rock", "spock"],
                    "rock": ["scissors", "lizard"],
                    "lizard": ["paper", "spock"],
                    "spock": ["rock", "scissors"],
                    "scissors": ["paper", "lizard"]
                };
                return gestureBeats[firstPlayerGesture].find(weakerGesture => weakerGesture === secondPlayerGesture);
            }(this.game.firstPlayer, this.game.secondPlayer);
            if (this.game.firstPlayer === this.game.secondPlayer) {
                this.game.firstPlayerWon = false;
                this.game.secondPlayerWon = false;
            } else {
                if (firstPlayerWin) {
                    this.game.firstPlayerWon = true;
                    this.game.secondPlayerWon = false;
                    this.game.score[0]++;
                } else {
                    this.game.firstPlayerWon = false;
                    this.game.secondPlayerWon = true;
                    this.game.score[1]++;
                }
            }
            this.game.status = gameStatus.RESULTS;
        }
    }

    remakeCheck(socketId) {
        if (this.game.hostId === socketId) {
            this.game.firstPlayerReady = true;
        } else {
            this.game.secondPlayerReady = true;
        }
        if (this.game.firstPlayerReady && this.game.secondPlayerReady) {
            this.game.status = gameStatus.CHOOSING;
            this.game.firstPlayer = "";
            this.game.secondPlayer = "";
            this.game.firstPlayerReady = false;
            this.game.secondPlayerReady = false;
        }
    }
}

module.exports = Game;