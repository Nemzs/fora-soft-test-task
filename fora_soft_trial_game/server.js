const app = require(__dirname + '/app')
const port = process.env.PORT || 8080;
const server = require('http').Server(app);
const socketHandler = require(__dirname + '/socketHandler.js');

server.listen(port, function () {
    console.log('Listening on port ', port)
});

let path = require('path');

app.get('/', function (req, res) {
    res.sendFile(path.resolve('public', 'index.html')) // Built React static files
});

let io = require('socket.io')(server);

socketHandler(io, 1000); //adding socket events to server, with limited as @args[1] game rooms at once
