const gameClass = require(__dirname + '/classes/Game.js');

function socketHandler(io, gamesLimit) {
    let gamesPool = [];

    io.on('connection', function (socket) {
        let game;
        let roomCreator = false;
        socket.on('createGame', function (sendHostURLtoClient) {
            roomCreator = true;

            const freeRoomId = function () {
                let roomId;

                for (let i = 0; i < gamesLimit; i++) {
                    const isFree = typeof gamesPool.find(game => game.getRoomName() === i.toString()) === 'undefined';
                    if (isFree) {
                        roomId = i.toString();
                        break;
                    }
                }
                if (roomId) {
                    return roomId;
                } else {
                    return -1;
                }
            };
            if (freeRoomId() >= 0) {
                const creatingGame = new gameClass(freeRoomId(), socket.id);

                gamesPool.push(creatingGame);
                game = gamesPool[gamesPool.length - 1];

                socket.join(game.getRoomName());

                sendHostURLtoClient({"url": getHostURL() + game.getRoomName()});
                console.log("created room " + game.getRoomName());
            } else {
                socket.emit("errorHandle", "All rooms are filled");
            }
        });
        socket.on('joinGame', function (clientId, roomId) {
            const suitableRoom = gamesPool.find(game => game.getRoomName() === roomId);
            if (suitableRoom) {
                if (!suitableRoom.roomIsFull()) {
                    game = suitableRoom;

                    socket.join(game.getRoomName());
                    game.playerConnected(clientId);

                    io.in(game.getRoomName()).emit('gameStateChanged', game.getData());
                } else {
                    socket.emit("errorHandle", "Room already full");
                }
            } else {
                socket.emit("errorHandle", "Game not found");
                console.log("no game")
            }
        });
        socket.on('chooseGist', function (type) {
            if (game) {
                game.setGesture(type, socket.id);
                io.in(game.getRoomName()).emit('gameStateChanged', game.getData());
            } else {
                console.log("Enter a game");
            }
        });
        socket.on("restartGameCheck", function () {
            if (game) {
                console.log(game);
                game.remakeCheck(socket.id);
                io.in(game.getRoomName()).emit('gameStateChanged', game.getData());
            } else {
                console.log("Enter a game");
            }
        });

        socket.on("disconnect", function () {
            if (roomCreator) {
                gamesPool = gamesPool.filter(currentGame => currentGame.getRoomName() !== game.getRoomName());
                io.in(game.getRoomName()).emit("errorHandle", "Host disconnected");
                console.log("kill room " + game.getRoomName());
                console.log("Host leaved")
            } else {
                if (game) {
                    game.playerDisconnected();
                    io.in(game.getRoomName()).emit('gameStateChanged', game.getData());
                    console.log("pause game");
                }
            }
        })
    });

    const getHostURL = function () {
        return "http://localhost:8080/"; //server base URL
    };
}


module.exports = socketHandler;