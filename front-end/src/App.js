import React, {Component} from 'react';
import {createGame, joinGame, gameStateChanged, chooseGist, restartGame, setErrorHandler} from "./api/gameHelper.js";
import {Button, Input} from 'reactstrap';
import GameInit from './components/GameInit'
import GestureMenu from './components/GestureMenu'
import MyScoreBoard from './components/MyScoreBoard'
import OpponentScoreBoard from './components/OpponentScoreBoard'
import RemakeMenu from './components/RemakeMenu'

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            gameStarted: false,
            game: {},
            hostURL: ""
        };
        this.handlePlayerChoice = this.handlePlayerChoice.bind(this);
        this.errorHandler = this.errorHandler.bind(this);
        this.getOpponentPlayer = this.getOpponentPlayer.bind(this);
        this.getMyPlayer = this.getMyPlayer.bind(this);
        this.restartGame = this.restartGame.bind(this);
    }

    gesturePictures = function () {
        return {
            rock: require('./img/rock.png'),
            paper: require('./img/paper.png'),
            scissors: require('./img/scissors.png'),
            lizard: require('./img/lizard.png'),
            spock: require('./img/spock.png'),
            unknown: require('./img/unknown.png')
        };
    }();


    componentDidMount() {
        const url = this.props.location.pathname.slice(1);
        setErrorHandler(this.errorHandler);
        if (url.length > 0) {
            this.setState({host: false});
            this.joinGame(url);
        } else {
            this.setState({host: true});
            this.hostGame();
        }
        gameStateChanged((error, state) => {
            if (state) {
                console.log(state);
                if (state.status === "waiting") {
                    this.setState({gameStarted: false, game: state})
                } else {
                    this.setState({gameStarted: true, game: state});
                }
            }
        })
    }

    hostGame = function () {
        createGame((response) => {
            if (response.url) {
                this.setState({hostURL: response.url});
            }
        });
        console.log(this.state);
    };

    joinGame = function (joinURL) {
        joinGame(joinURL);
    };

    restartGame = function () {
        return (event) => {
            restartGame((id) => {
                console.log(id);
            })
        }
    };

    errorHandler = function (errorMessage) {
        console.log(errorMessage);
        this.setState({error: errorMessage});
    };

    handlePlayerChoice = function (type) {
        return (event) => {
            if (this.state.game.status === "choosing") {
                chooseGist(type);
            }
        }
    };


    getPlayer = function (isHost) {
        if (isHost) {
            const won = this.state.game.firstPlayerWon;
            const score = this.state.game.score ? this.state.game.score[0] : 0;
            return {
                gesture: this.state.game.firstPlayer, ready: this.state.game.firstPlayerReady, won: won,
                checkReady: this.state.game.firstPlayerReady, score: score
            };
        } else {
            const won = this.state.game.secondPlayerWon;
            const score = this.state.game.score ? this.state.game.score[1] : 0;
            return {
                gesture: this.state.game.secondPlayer, ready: this.state.game.secondPlayer, won: won,
                checkReady: this.state.game.secondPlayerReady, score: score
            };
        }
    };

    getMyPlayer = function () {
        return this.getPlayer(this.state.host);
    };

    getOpponentPlayer = function () {
        console.log(this.getPlayer(!this.state.host));
        return this.getPlayer(!this.state.host);
    };


    render() {
        const gameHeader = function (gameStatus, myPlayer, opponentPlayer) {
            if (gameStatus === "choosing") {
                const playerMadeChoise = myPlayer.gesture.length !== 0;

                return playerMadeChoise ? "Waiting for opponent gesture" : "Choose gesture";
            } else {
                if (!myPlayer.won && !opponentPlayer.won) {
                    return "Draw!";
                } else {
                    return myPlayer.won ? "You won" : "Try again!";
                }
            }
        }(this.state.game.status, this.getMyPlayer(), this.getOpponentPlayer());

        return (
            <div className="mainSheet">
                {this.state.error ?
                    <p className="centerText">{this.state.error}</p> :
                    <div>
                        {!this.state.gameStarted ?
                            <GameInit playerIsHost={this.state.host} hostURL={this.state.hostURL}/> :
                            <div>
                                <h3 className="gameHeader">{gameHeader}</h3>
                                <GestureMenu getMyPlayer={this.getMyPlayer} getOpponentPlayer={this.getOpponentPlayer}
                                             gameStatus={this.state.game.status}
                                             choiceGesture={this.handlePlayerChoice}
                                             gesturePictures={this.gesturePictures}/>
                                <br/>
                                <div className="scoreBoard">
                                    <p className="scoreHeader">Results</p>
                                    <MyScoreBoard getMyPlayer={this.getMyPlayer}/>
                                    <OpponentScoreBoard getOpponentPlayer={this.getOpponentPlayer}/>
                                    <RemakeMenu gameStatus={this.state.game.status} getMyPlayer={this.getMyPlayer}
                                                remake={this.restartGame}/>

                                </div>
                            </div>
                        }
                    </div>}
            </div>
        );
    }
}

export default App;
