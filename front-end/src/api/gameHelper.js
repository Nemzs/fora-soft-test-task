import openSocket from 'socket.io-client';

const socket = openSocket('http://localhost:8080');

function createGame(cb) {
    socket.emit('createGame', cb);
}

function joinGame(joinURL) {
    socket.emit('joinGame', socket.id, joinURL);
}

function gameStateChanged(handleStateChangeCallback) {
    socket.on('gameStateChanged', gameState => handleStateChangeCallback(null, gameState));
}

function setErrorHandler(errorHandler){
    socket.on('errorHandle', error => errorHandler(error));
}

function chooseGist(type) {
    socket.emit("chooseGist", type)
}

function restartGame(){
    socket.emit("restartGameCheck");
}

export {createGame, joinGame, gameStateChanged, chooseGist, restartGame, setErrorHandler};