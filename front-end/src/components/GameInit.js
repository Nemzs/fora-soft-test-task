import React, {Component} from 'react';
import {Button, Input} from 'reactstrap';
import copy from 'copy-to-clipboard';

class GameInit extends Component {
    constructor(props) {
        super(props);
        this.state = {copySuccess: ''};
    }

    copyToClipboard = (e) => {
        copy(this.props.hostURL);
        this.setState({copySuccess: 'Copied!'});
    };

    render() {
        return <div>
            {this.props.playerIsHost ?
                <div className="inviteBox">
                    <p>Share link</p>
                    <Input className="hostURLInput" value={this.props.hostURL}></Input>

                    <Button outline onClick={this.copyToClipboard}>Copy</Button>
                    <br></br>
                    {this.state.copySuccess}
                </div> :
                <div>
                    <p className="centerText">Connecting to game</p>
                </div>
            }
        </div>
    }
}

export default GameInit;