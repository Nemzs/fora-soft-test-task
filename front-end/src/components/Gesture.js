import React, {Component} from 'react';

class Gesture extends Component {
    constructor(props) {
        super(props);
        console.log(props);
    }

    render() {
        const picDashboardClass = (type) => {
            console.log(this.state);
            if (this.props.gameStatus === "choosing") {
                if (this.props.activeGesture === type) {
                    return "highlightPic";
                } else {
                    return "pic";
                }
            } else {
                if (this.props.activeGesture === type) {
                    return "chosen";
                } else {
                    return "blur";
                }
            }
        };
        return <img className={picDashboardClass(this.props.name)}
                    onClick={this.props.choiceGesture(this.props.name)}
                    src={this.props.gesturePictures[this.props.name]}/>
    }
}

export default Gesture;