import React, {Component} from 'react';
import Gesture from './Gesture'
import Results from './Results'

class GestureMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gestureList: ["rock", "paper", "scissors", "lizard", "spock"]
        };
    }

    render() {
        return <div className="gestureBlock">
            {this.state.gestureList.map(gesture => {
                return <Gesture name={gesture}
                                choiceGesture={this.props.choiceGesture}
                                activeGesture={this.props.getMyPlayer().gesture}
                                gameStatus={this.props.gameStatus}
                                gesturePictures={this.props.gesturePictures}/>
            })}
            <Results gesturePictures={this.props.gesturePictures}
                     gameStatus={this.props.gameStatus} getOpponentPlayer={this.props.getOpponentPlayer}/>
        </div>
    }
}

export default GestureMenu;