import React, {Component} from 'react';

class MyScoreBoard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="myScore">
            {this.props.getMyPlayer().checkReady ?
                <span><span
                    className="text-success">Me</span> ( {this.props.getMyPlayer().score} )</span> :
                <span>Me ( {this.props.getMyPlayer().score} )</span>
            }
        </div>
    }
}

export default MyScoreBoard;