import React, {Component} from 'react';

class OpponentScoreBoard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="opponentScore">
            {this.props.getOpponentPlayer().checkReady ?
                <span><span
                    className="text-success">Opponent</span> ( {this.props.getOpponentPlayer().score} )</span> :
                <span>Opponent ( {this.props.getOpponentPlayer().score} )</span>
            }
        </div>
    }
}

export default OpponentScoreBoard;