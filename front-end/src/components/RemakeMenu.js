import React, {Component} from 'react';
import {Button} from 'reactstrap';

class RemakeMenu extends Component {
    constructor(props) {
        super(props);
        console.log(this.props);
    }

    render() {
        return <div className="remakeMenu">
            {this.props.gameStatus === "results" && !this.props.getMyPlayer().checkReady &&
            <Button outline className={"remakeButton"}
                    onClick={this.props.remake()}>Remake</Button>
            }
        </div>
    }
}

export default RemakeMenu;