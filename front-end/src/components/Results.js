import React, {Component} from 'react';

class Results extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="resultsBlock">
            <span>vs</span>
            {this.props.gameStatus === "choosing" ?
                <div className="opponentGestureBlock">
                    {this.props.getOpponentPlayer().gesture.length === 0 ?
                        <img className="unknownPic"
                             src={this.props.gesturePictures["unknown"]}/>
                        :
                        <img className="highlightPic"
                             src={this.props.gesturePictures["unknown"]}/>}
                </div> :
                <div className="opponentGestureBlock">
                    <img className="pic"
                         src={this.props.gesturePictures[this.props.getOpponentPlayer().gesture]}/>
                </div>}
        </div>
    }
}

export default Results;